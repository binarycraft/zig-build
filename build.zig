const std = @import("std");
const ell_builder = @import("libs/ell/build.zig");
const tinfow_builder = @import("libs/libtinfow/build.zig");
const readline_builder = @import("libs/readline/build.zig");

pub fn build(b: *std.build.Builder) !void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    const mode = b.standardReleaseOptions();

    const ell = try ell_builder.build(b, mode, target);
    const tinfow = try tinfow_builder.build(b, mode, target);
    const readline = try readline_builder.build(b, mode, target);

    const iwd_src = [_][]const u8{
        "libs/iwd/upstream/src/main.c",
        "libs/iwd/upstream/src/netdev.c",
        "libs/iwd/upstream/src/wiphy.c",
        "libs/iwd/upstream/src/device.c",
        "libs/iwd/upstream/src/station.c",
        "libs/iwd/upstream/src/ie.c",
        "libs/iwd/upstream/src/dbus.c",
        "libs/iwd/upstream/src/mpdu.c",
        "libs/iwd/upstream/src/eapol.c",
        "libs/iwd/upstream/src/eapolutil.c",
        "libs/iwd/upstream/src/handshake.c",
        "libs/iwd/upstream/src/scan.c",
        "libs/iwd/upstream/src/common.c",
        "libs/iwd/upstream/src/agent.c",
        "libs/iwd/upstream/src/storage.c",
        "libs/iwd/upstream/src/network.c",
        "libs/iwd/upstream/src/wsc.c",
        "libs/iwd/upstream/src/backtrace.c",
        "libs/iwd/upstream/src/knownnetworks.c",
        "libs/iwd/upstream/src/rfkill.c",
        "libs/iwd/upstream/src/ft.c",
        "libs/iwd/upstream/src/ap.c",
        "libs/iwd/upstream/src/adhoc.c",
        "libs/iwd/upstream/src/sae.c",
        "libs/iwd/upstream/src/nl80211util.c",
        "libs/iwd/upstream/src/nl80211cmd.c",
        "libs/iwd/upstream/src/owe.c",
        "libs/iwd/upstream/src/blacklist.c",
        "libs/iwd/upstream/src/manager.c",
        "libs/iwd/upstream/src/erp.c",
        "libs/iwd/upstream/src/fils.c",
        "libs/iwd/upstream/src/anqp.c",
        "libs/iwd/upstream/src/anqputil.c",
        "libs/iwd/upstream/src/netconfig.c",
        "libs/iwd/upstream/src/netconfig-commit.c",
        "libs/iwd/upstream/src/resolve.c",
        "libs/iwd/upstream/src/hotspot.c",
        "libs/iwd/upstream/src/p2p.c",
        "libs/iwd/upstream/src/p2putil.c",
        "libs/iwd/upstream/src/module.c",
        "libs/iwd/upstream/src/rrm.c",
        "libs/iwd/upstream/src/frame-xchg.c",
        "libs/iwd/upstream/src/eap-wsc.c",
        "libs/iwd/upstream/src/wscutil.c",
        "libs/iwd/upstream/src/diagnostic.c",
        "libs/iwd/upstream/src/ip-pool.c",
        "libs/iwd/upstream/src/band.c",
        "libs/iwd/upstream/src/sysfs.c",
        "libs/iwd/upstream/src/offchannel.c",
        "libs/iwd/upstream/src/dpp-util.c",
        "libs/iwd/upstream/src/json.c",
        "libs/iwd/upstream/src/dpp.c",
        "libs/iwd/upstream/src/ofono.c",
    };

    const iwctl_src = [_][]const u8{
        "libs/iwd/upstream/client/main.c",
        "libs/iwd/upstream/client/adapter.c",
        "libs/iwd/upstream/client/agent.c",
        "libs/iwd/upstream/client/agent-manager.c",
        "libs/iwd/upstream/client/ad-hoc.c",
        "libs/iwd/upstream/client/ap.c",
        "libs/iwd/upstream/client/command.c",
        "libs/iwd/upstream/client/dbus-proxy.c",
        "libs/iwd/upstream/client/device.c",
        "libs/iwd/upstream/client/display.c",
        "libs/iwd/upstream/client/known-networks.c",
        "libs/iwd/upstream/client/network.c",
        "libs/iwd/upstream/client/properties.c",
        "libs/iwd/upstream/client/wsc.c",
        "libs/iwd/upstream/client/station.c",
        "libs/iwd/upstream/client/diagnostic.c",
        "libs/iwd/upstream/client/daemon.c",
        "libs/iwd/upstream/client/dpp.c",
        "libs/iwd/upstream/client/station-debug.c",
        "libs/iwd/upstream/src/util.c",
        "libs/iwd/upstream/src/band.c",
    };

    const iwmon_src = [_][]const u8{
        "libs/iwd/upstream/monitor/main.c",
        "libs/iwd/upstream/monitor/nlmon.c",
        "libs/iwd/upstream/monitor/pcap.c",
        "libs/iwd/upstream/monitor/display.c",
        "libs/iwd/upstream/src/ie.c",
        "libs/iwd/upstream/src/wscutil.c",
        "libs/iwd/upstream/src/mpdu.c",
        "libs/iwd/upstream/src/util.c",
        "libs/iwd/upstream/src/crypto.c",
        "libs/iwd/upstream/src/watchlist.c",
        "libs/iwd/upstream/src/eapolutil.c",
        "libs/iwd/upstream/src/nl80211cmd.c",
        "libs/iwd/upstream/src/p2putil.c",
        "libs/iwd/upstream/src/anqputil.c",
        "libs/iwd/upstream/src/band.c",
    };

    const ead_src = [_][]const u8{
        "libs/iwd/upstream/wired/main.c",
        "libs/iwd/upstream/wired/ethdev.c",
        "libs/iwd/upstream/wired/network.c",
        "libs/iwd/upstream/wired/dbus.c",
        "libs/iwd/upstream/src/module.c",
        "libs/iwd/upstream/src/band.c",
    };

    const eap_src = [_][]const u8{
        "libs/iwd/upstream/src/eap.c",
        "libs/iwd/upstream/src/eap-md5.c",
        "libs/iwd/upstream/src/eap-tls.c",
        "libs/iwd/upstream/src/eap-ttls.c",
        "libs/iwd/upstream/src/eap-mschapv2.c",
        "libs/iwd/upstream/src/eap-sim.c",
        "libs/iwd/upstream/src/eap-aka.c",
        "libs/iwd/upstream/src/eap-peap.c",
        "libs/iwd/upstream/src/eap-gtc.c",
        "libs/iwd/upstream/src/eap-pwd.c",
        "libs/iwd/upstream/src/util.c",
        "libs/iwd/upstream/src/crypto.c",
        "libs/iwd/upstream/src/simutil.c",
        "libs/iwd/upstream/src/simauth.c",
        "libs/iwd/upstream/src/watchlist.c",
        "libs/iwd/upstream/src/eap-tls-common.c",
        "libs/iwd/upstream/src/mschaputil.c",
    };

    const probe_req_src = [_][]const u8{
        "libs/iwd/upstream/tools/probe-req.c",
        "libs/iwd/upstream/src/mpdu.c",
        "libs/iwd/upstream/src/ie.c",
        "libs/iwd/upstream/src/nl80211util.c",
        "libs/iwd/upstream/src/util.c",
        "libs/iwd/upstream/src/common.c",
        "libs/iwd/upstream/src/band.c",
    };

    const decrypt_profile_src = [_][]const u8{
        "libs/iwd/upstream/tools/iwd-decrypt-profile.c",
        "libs/iwd/upstream/src/common.c",
        "libs/iwd/upstream/src/crypto.c",
        "libs/iwd/upstream/src/storage.c",
    };

    const hwsim_src = [_][]const u8{
        "libs/iwd/upstream/tools/hwsim.c",
        "libs/iwd/upstream/src/util.c",
        "libs/iwd/upstream/src/nl80211cmd.c",
        "libs/iwd/upstream/src/nl80211util.c",
        "libs/iwd/upstream/src/storage.c",
        "libs/iwd/upstream/src/common.c",
        "libs/iwd/upstream/src/band.c",
        "libs/iwd/upstream/src/crypto.c",
    };

    var cflags = std.ArrayList([]const u8).init(b.allocator);
    try cflags.appendSlice(&.{
        "-DDAEMON_CONFIGDIR=\"/etc/iwd\"",
        "-DDAEMON_STORAGEDIR=\"/var/lib/iwd\"",
        "-DHAVE_DLFCN_H=1",
        "-DHAVE_EXPLICIT_BZERO=1",
        "-DHAVE_INTTYPES_H=1",
        "-DHAVE_LIBASAN=1",
        "-DHAVE_LIBLSAN=1",
        "-DHAVE_LIBUBSAN=1",
        "-DHAVE_LINUX_IF_ALG_H=1",
        "-DHAVE_LINUX_TYPES_H=1",
        "-DHAVE_READLINE_READLINE_H=1",
        "-DHAVE_REALLOCARRAY=1",
        "-DHAVE_STDINT_H=1",
        "-DHAVE_STDIO_H=1",
        "-DHAVE_STDLIB_H=1",
        "-DHAVE_STRINGS_H=1",
        "-DHAVE_STRING_H=1",
        "-DHAVE_SYS_STAT_H=1",
        "-DHAVE_SYS_TYPES_H=1",
        "-DHAVE_UNISTD_H=1",
        "-DSTDC_HEADERS=1",
        "-DVERSION=\"2.0\"",
        "-DWIRED_STORAGEDIR=\"/var/lib/ead\"",
        "-fvisibility=hidden",
        "-DUNITDIR=\"unit\"",
        "-DCERTDIR=\"unit\"",
        "-DJSMN_PARENT_LINKS",
        "-DJSMN_STRICT",
        "-Wall",
        "-fsigned-char",
        "-fno-exceptions",
        "-U_FORTIFY_SOURCE",
        "-D_FORTIFY_SOURCE=2",
    });

    if (target.os_tag) |_| {
        if (target.isGnuLibC()) {
            try cflags.append("-DHAVE_RAWMEMCHR=1");
        }

        if (target.toTarget().cpu.arch == .aarch64) {
            try cflags.append("-mno-outline-atomics");
        } else {
            try cflags.append("-fcf-protection");
        }
    } else {
        if (b.host.target.isGnuLibC()) {
            try cflags.append("-DHAVE_RAWMEMCHR=1");
        }

        if (b.host.target.cpu.arch == .aarch64) {
            try cflags.append("-mno-outline-atomics");
        } else {
            try cflags.append("-fcf-protection");
        }
    }

    const iwd = b.addExecutable("iwd", null);
    iwd.linkLibC();
    iwd.addIncludePath("zig-out/lib/ell/include");
    iwd.addIncludePath("libs/iwd/upstream");
    iwd.linkLibrary(ell);
    iwd.addCSourceFiles(&eap_src, cflags.items);
    iwd.addCSourceFiles(&iwd_src, cflags.items);
    iwd.setTarget(target);
    iwd.setBuildMode(mode);
    iwd.install();

    const iwctl = b.addExecutable("iwctl", null);
    iwctl.linkLibC();
    iwctl.addIncludePath("zig-out/lib/ell/include");
    iwctl.addIncludePath("libs/iwd/upstream");
    iwctl.addIncludePath("zig-out/lib/readline/include");
    iwctl.linkLibrary(readline);
    iwctl.linkLibrary(tinfow);
    iwctl.linkLibrary(ell);
    iwctl.addCSourceFiles(&iwctl_src, cflags.items);
    iwctl.setTarget(target);
    iwctl.setBuildMode(mode);
    iwctl.install();

    const iwmon = b.addExecutable("iwmon", null);
    iwmon.linkLibC();
    iwmon.addIncludePath("zig-out/lib/ell/include");
    iwmon.addIncludePath("libs/iwd/upstream");
    iwmon.linkLibrary(ell);
    iwmon.addCSourceFiles(&iwmon_src, cflags.items);
    iwmon.setTarget(target);
    iwmon.setBuildMode(mode);
    iwmon.install();

    const ead = b.addExecutable("ead", null);
    ead.linkLibC();
    ead.addIncludePath("zig-out/lib/ell/include");
    ead.addIncludePath("libs/iwd/upstream");
    ead.linkLibrary(ell);
    ead.addCSourceFiles(&eap_src, cflags.items);
    ead.addCSourceFiles(&ead_src, cflags.items);
    ead.setTarget(target);
    ead.setBuildMode(mode);
    ead.install();

    const probe_req = b.addExecutable("probe-req", null);
    probe_req.linkLibC();
    probe_req.addIncludePath("zig-out/lib/ell/include");
    probe_req.addIncludePath("libs/iwd/upstream");
    probe_req.linkLibrary(ell);
    probe_req.addCSourceFiles(&probe_req_src, cflags.items);
    probe_req.setTarget(target);
    probe_req.setBuildMode(mode);
    probe_req.install();

    const decrypt_profile = b.addExecutable("decrypt-profile", null);
    decrypt_profile.linkLibC();
    decrypt_profile.addIncludePath("zig-out/lib/ell/include");
    decrypt_profile.addIncludePath("libs/iwd/upstream");
    decrypt_profile.linkLibrary(ell);
    decrypt_profile.addCSourceFiles(&decrypt_profile_src, cflags.items);
    decrypt_profile.setTarget(target);
    decrypt_profile.setBuildMode(mode);
    decrypt_profile.install();

    const hwsim = b.addExecutable("hwsim", null);
    hwsim.linkLibC();
    hwsim.addIncludePath("zig-out/lib/ell/include");
    hwsim.addIncludePath("libs/iwd/upstream");
    hwsim.linkLibrary(ell);
    hwsim.addCSourceFiles(&hwsim_src, cflags.items);
    hwsim.setTarget(target);
    hwsim.setBuildMode(mode);
    hwsim.install();
}
