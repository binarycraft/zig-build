const std = @import("std");

pub fn build(
    b: *std.build.Builder,
    mode: std.builtin.Mode,
    target: std.zig.CrossTarget,
) !*std.build.LibExeObjStep {
    const ell_src = [_][]const u8{
        "libs/ell/upstream/ell/util.c",
        "libs/ell/upstream/ell/test.c",
        "libs/ell/upstream/ell/strv.c",
        "libs/ell/upstream/ell/utf8.c",
        "libs/ell/upstream/ell/queue.c",
        "libs/ell/upstream/ell/hashmap.c",
        "libs/ell/upstream/ell/string.c",
        "libs/ell/upstream/ell/settings.c",
        "libs/ell/upstream/ell/main.c",
        "libs/ell/upstream/ell/idle.c",
        "libs/ell/upstream/ell/signal.c",
        "libs/ell/upstream/ell/timeout.c",
        "libs/ell/upstream/ell/io.c",
        "libs/ell/upstream/ell/ringbuf.c",
        "libs/ell/upstream/ell/log.c",
        "libs/ell/upstream/ell/checksum.c",
        "libs/ell/upstream/ell/netlink.c",
        "libs/ell/upstream/ell/genl.c",
        "libs/ell/upstream/ell/rtnl.c",
        "libs/ell/upstream/ell/dbus.c",
        "libs/ell/upstream/ell/dbus-message.c",
        "libs/ell/upstream/ell/dbus-util.c",
        "libs/ell/upstream/ell/dbus-service.c",
        "libs/ell/upstream/ell/dbus-client.c",
        "libs/ell/upstream/ell/dbus-name-cache.c",
        "libs/ell/upstream/ell/dbus-filter.c",
        "libs/ell/upstream/ell/gvariant-util.c",
        "libs/ell/upstream/ell/siphash.c",
        "libs/ell/upstream/ell/hwdb.c",
        "libs/ell/upstream/ell/cipher.c",
        "libs/ell/upstream/ell/random.c",
        "libs/ell/upstream/ell/uintset.c",
        "libs/ell/upstream/ell/base64.c",
        "libs/ell/upstream/ell/pem.c",
        "libs/ell/upstream/ell/tls.c",
        "libs/ell/upstream/ell/tls-record.c",
        "libs/ell/upstream/ell/tls-extensions.c",
        "libs/ell/upstream/ell/tls-suites.c",
        "libs/ell/upstream/ell/uuid.c",
        "libs/ell/upstream/ell/key.c",
        "libs/ell/upstream/ell/file.c",
        "libs/ell/upstream/ell/dir.c",
        "libs/ell/upstream/ell/net.c",
        "libs/ell/upstream/ell/dhcp.c",
        "libs/ell/upstream/ell/dhcp-transport.c",
        "libs/ell/upstream/ell/dhcp-lease.c",
        "libs/ell/upstream/ell/dhcp6.c",
        "libs/ell/upstream/ell/dhcp6-transport.c",
        "libs/ell/upstream/ell/dhcp6-lease.c",
        "libs/ell/upstream/ell/dhcp-util.c",
        "libs/ell/upstream/ell/dhcp-server.c",
        "libs/ell/upstream/ell/cert.c",
        "libs/ell/upstream/ell/cert-crypto.c",
        "libs/ell/upstream/ell/ecc-external.c",
        "libs/ell/upstream/ell/ecc.c",
        "libs/ell/upstream/ell/ecdh.c",
        "libs/ell/upstream/ell/time.c",
        "libs/ell/upstream/ell/gpio.c",
        "libs/ell/upstream/ell/path.c",
        "libs/ell/upstream/ell/icmp6.c",
        "libs/ell/upstream/ell/acd.c",
        "libs/ell/upstream/ell/tester.c",
        "libs/ell/upstream/ell/netconfig.c",
    };

    var cflags = std.ArrayList([]const u8).init(b.allocator);
    try cflags.appendSlice(&.{
        "-DHAVE_DLFCN_H=1",
        "-DHAVE_EXPLICIT_BZERO=1",
        "-DHAVE_INTTYPES_H=1",
        "-DHAVE_LIBASAN=1",
        "-DHAVE_LIBLSAN=1",
        "-DHAVE_LIBUBSAN=1",
        "-DHAVE_LINUX_IF_ALG_H=1",
        "-DHAVE_LINUX_TYPES_H=1",
        "-DHAVE_READLINE_READLINE_H=1",
        "-DHAVE_REALLOCARRAY=1",
        "-DHAVE_STDINT_H=1",
        "-DHAVE_STDIO_H=1",
        "-DHAVE_STDLIB_H=1",
        "-DHAVE_STRINGS_H=1",
        "-DHAVE_STRING_H=1",
        "-DHAVE_SYS_STAT_H=1",
        "-DHAVE_SYS_TYPES_H=1",
        "-DHAVE_UNISTD_H=1",
        "-DSTDC_HEADERS=1",
        "-DVERSION=\"0.54\"",
        "-fvisibility=hidden",
        "-DUNITDIR=\"unit\"",
        "-DCERTDIR=\"unit\"",
        "-DJSMN_PARENT_LINKS",
        "-DJSMN_STRICT",
        "-Wall",
        "-fsigned-char",
        "-fno-exceptions",
        "-U_FORTIFY_SOURCE",
        "-D_FORTIFY_SOURCE=2",
    });

    if (target.os_tag) |_| {
        if (target.isGnuLibC()) {
            try cflags.append("-DHAVE_RAWMEMCHR=1");
        }

        if (target.toTarget().cpu.arch == .aarch64) {
            try cflags.append("-mno-outline-atomics");
        } else {
            try cflags.append("-fcf-protection");
        }
    } else {
        if (b.host.target.isGnuLibC()) {
            try cflags.append("-DHAVE_RAWMEMCHR=1");
        }

        if (b.host.target.cpu.arch == .aarch64) {
            try cflags.append("-mno-outline-atomics");
        } else {
            try cflags.append("-fcf-protection");
        }
    }

    const ell = b.addStaticLibrary("ell", null);
    ell.linkLibC();
    ell.addIncludePath("libs/ell/upstream");
    ell.addCSourceFiles(&ell_src, cflags.items);
    ell.setTarget(target);
    ell.setBuildMode(mode);
    b.installDirectory(std.build.InstallDirectoryOptions{
        .source_dir = "libs/ell/upstream/ell",
        .install_dir = .lib,
        .install_subdir = "ell/include/ell",
        .exclude_extensions = &[_][]const u8{
            ".c",
        },
    });
    return ell;
}
